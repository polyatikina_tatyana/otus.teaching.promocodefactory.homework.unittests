﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Providers;
using System;

namespace Otus.Teaching.PromoCodeFactory.Domain.Providers
{
    public class CurrentDateTimeProvider : ICurrentDateTimeProvider
    {
        public DateTime CurrentDateTime => DateTime.Now;
    }
}
