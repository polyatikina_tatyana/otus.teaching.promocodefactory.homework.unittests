﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System.Collections.Generic;
using System;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Builders
{
    public static class PartnersBuilder
    {
        public static Partner CreateBasePartner()
        {
            var partner = new Partner()
            {
                Id = Guid.Parse("277D0CAF-6C9D-4465-9661-9719A777FF65"),
                Name = "ООО Малиновый рай",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("277D0CAF-6C9D-4465-9661-9719A777FF65"),
                        CreateDate = new DateTime(2023, 01, 9),
                        EndDate = new DateTime(2023, 10, 9),
                        Limit = 100
                    }
                }
            };

            return partner;
        }

        public static Partner SetNotActive(this Partner partner)
        {
            partner.IsActive = false;
            return partner;
        }

        public static Partner SetNotActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("4418D0AD-C0C7-40E8-A280-3D9981C0D082"),
                        CreateDate = new DateTime(2023, 01, 9),
                        EndDate = new DateTime(2023, 10, 9),
                        CancelDate = new DateTime(2023, 02, 22),
                        Limit = 100
                    }
            };

            return partner;
        }

        public static Partner SetActiveLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("A2998FA2-ED6A-44E8-A715-77F517B66B9A"),
                        CreateDate = new DateTime(2023, 02, 22),
                        EndDate = new DateTime(2023, 12, 9),
                        Limit = 100
                    }
            };
            partner.NumberIssuedPromoCodes = 50;

            return partner;
        }

        public static Partner ClearNumberIssuedPromoCodes(this Partner partner)
        {
            partner.NumberIssuedPromoCodes = 50;

            return partner;
        }

        public static Partner SetNegativeLimit(this Partner partner)
        {
            partner.PartnerLimits = new List<PartnerPromoCodeLimit>
            {
                new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("58FE5E2C-3184-426C-AB94-30ADA929EC41"),
                        CreateDate = new DateTime(2023, 02, 22),
                        EndDate = new DateTime(2023, 12, 9),
                        CancelDate = new DateTime(2023, 03, 22),
                        Limit = -100
                    }
            };
            return partner;
        }
    }
}
