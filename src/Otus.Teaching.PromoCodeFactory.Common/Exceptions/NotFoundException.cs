﻿using System;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Common.Exceptions
{
    //https://rules.sonarsource.com/csharp/RSPEC-3925
    [Serializable]
    public sealed class NotFoundException : Exception
    {
        public NotFoundException() : this("С данным ключом значение не найдено") { }

        public NotFoundException(string message)
            : base(message)
        {
        }

        private NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context) => base.GetObjectData(info, context);
    }
}
