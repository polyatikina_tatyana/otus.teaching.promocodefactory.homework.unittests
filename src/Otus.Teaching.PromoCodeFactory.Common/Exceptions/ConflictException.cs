﻿using System;
using System.Runtime.Serialization;

namespace Otus.Teaching.PromoCodeFactory.Common.Exceptions
{
    //https://rules.sonarsource.com/csharp/RSPEC-3925
    [Serializable]
    public sealed class ConflictException : Exception
    {
        public ConflictException() : this("С данным ключом значение добавлено ранее") { }

        public ConflictException(string message)
            : base(message)
        {
        }

        private ConflictException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public override void GetObjectData(SerializationInfo info, StreamingContext context) => base.GetObjectData(info, context);
    }
}
