﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Common.Exceptions;
using Otus.Teaching.PromoCodeFactory.Domain.Managers;
using Otus.Teaching.PromoCodeFactory.Domain.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Партнеры
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PartnersController
        : ControllerBase
    {
        private readonly PartnersManager _partnersManager;

        public PartnersController(PartnersManager partnersManager)
        {
            _partnersManager = partnersManager;
        }

        [HttpGet]
        public async Task<ActionResult<List<PartnerResponse>>> GetPartnersAsync()
        {
            var response =  await _partnersManager.GetPartnersAsync();
            return Ok(response);
        }

        [HttpGet("{id}")]
        public Task<ActionResult<PartnerResponse>> GetPartnerByIdAsync(Guid id)
        {
            return ExecuteAsync<PartnerResponse>(async () =>
            {
                var response = await _partnersManager.GetPartnerByIdAsync(id);
                return Ok(response);
            });
        }


        [HttpGet("{id}/limits/{limitId}")]
        public Task<ActionResult<PartnerPromoCodeLimitResponse>> GetPartnerLimitAsync(Guid id, Guid limitId)
        {
            return ExecuteAsync<PartnerPromoCodeLimitResponse>(async () =>
            {
                var response = await _partnersManager.GetPartnerLimitAsync(id, limitId);

                if (response == null)
                    return NoContent();

                return Ok(response);
            });
        }
        
        [HttpPost("{id}/limits")]
        public Task<IActionResult> SetPartnerPromoCodeLimitAsync(Guid id, SetPartnerPromoCodeLimitRequest request)
        {
            return ExecuteAsync(async () =>
            {
                var (partnerId, newLimitId) = await _partnersManager.SetPartnerPromoCodeLimitAsync(id, request);

                return CreatedAtAction(nameof(GetPartnerLimitAsync), new { id = partnerId, limitId = newLimitId }, null);
            });
        }

        [HttpPost("{id}/canceledLimits")]
        public Task<IActionResult> CancelPartnerPromoCodeLimitAsync(Guid id)
        {
            return ExecuteAsync(async () =>
            {
                await _partnersManager.CancelPartnerPromoCodeLimitAsync(id);

                return NoContent();
            });
        }

        private async Task<IActionResult> ExecuteAsync(Func<Task<IActionResult>> func)
        {
            try
            {
                return await func.Invoke();

            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        private async Task<ActionResult<T>> ExecuteAsync<T>(Func<Task<ActionResult<T>>> func)
        {
            try
            {
                return await func.Invoke();

            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}